-- main = Read list of directories and their sizes.
--        Decide how to fit them on CD-Rs.
--        Print solution.
module Main where

main = do input <- getContents
  putStrLn ("DEBUG: got input" ++ input)
  -- compute solution and print it
